plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)
}

// Definition der Abhängigkeit für AppCompat
val appCompatDependency = "androidx.appcompat:appcompat:1.4.1"

android {
    namespace = "com.example.my_s_app.android"
    compileSdk = 34
    defaultConfig {
        applicationId = "com.example.my_s_app.android"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"
    }
    buildFeatures {

    }

    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(projects.shared)
    implementation(libs.androidx.constraintlayout)

    // Hinzufügen der AppCompat-Abhängigkeit
    implementation(appCompatDependency)
}
