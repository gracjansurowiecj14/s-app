package com.example.my_s_app

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform